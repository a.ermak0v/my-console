<?php

namespace MyConsole\Input;

use MyConsole\Helpers\ParseArgvHelper;

/**
 * Класс ввода, служит для получения названия вводимой команды, а так же аргументов и параметров
 */
class Input implements InputInterface
{
    protected ?string $commandName = null;

    protected ?array $arguments = null;

    protected ?array $options = null;

    public function __construct(?array $argv = null)
    {
        $argv = $argv ?? $_SERVER['argv'] ?? [];
        $this->parse($argv);
    }

    public function parse(array $argv): void
    {
        if ($argv) {
            [$this->commandName, $this->arguments, $this->options] = ParseArgvHelper::parse($argv);
        }
    }

    public function getCommandName(): ?string
    {
        return $this->commandName;
    }

    public function setCommandName(?string $commandName): self
    {
        $this->commandName = $commandName;
        return $this;
    }

    public function getArguments(): ?array
    {
        return $this->arguments;
    }

    public function setArguments(?array $arguments): self
    {
        $this->arguments = $arguments;
        return $this;
    }

    public function getOptions(): ?array
    {
        return $this->options;
    }

    public function setOptions(?array $options): self
    {
        $this->options = $options;
        return $this;
    }
}
