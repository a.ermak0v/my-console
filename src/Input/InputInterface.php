<?php

namespace MyConsole\Input;

interface InputInterface
{
    public function parse(array $argv): void;

    public function getCommandName(): ?string;

    public function setCommandName(?string $commandName): self;

    public function getArguments(): ?array;

    public function setArguments(?array $arguments): self;

    public function getOptions(): ?array;

    public function setOptions(?array $options): self;
}
