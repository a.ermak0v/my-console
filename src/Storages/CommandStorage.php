<?php

namespace MyConsole\Storages;

class CommandStorage
{
    public const SUCCESS = 0;
    public const FAILURE = 1;
    public const INVALID = 2;
}
