<?php

namespace MyConsole\Output;

/**
 * Класс для ввывода данных в консоль
 */
class Output implements OutputInterface
{
    public function write(string $string): void
    {
        echo $string;
    }

    public function writeln(?string $string = null): void
    {
        if ($string) {
            $this->write($string);
        }
        echo PHP_EOL;
    }
}
