<?php

namespace MyConsole\Output;

interface OutputInterface
{
    public function write(string $string): void;

    public function writeln(string $string): void;
}
