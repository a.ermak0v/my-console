<?php

namespace MyConsole;

use MyConsole\Commands\Command;
use MyConsole\Exceptions\AddCommandException;
use MyConsole\Exceptions\CommandNotFoundException;
use MyConsole\Input\Input;
use MyConsole\Output\Output;

/**
 * Основной класс приложения
 */
class Application
{
    /** @var Command[] */
    private array $commands = [];

    private Input $input;

    private Output $output;

    public function __construct()
    {
        $this->init();
    }

    public function addCommand(Command $command): self
    {
        $commandName = $command->getName();
        if (isset($this->commands[$commandName])) {
            throw new AddCommandException('Registering an existing command - ' . $commandName);
        }

        $this->commands[$commandName] = $command
            ->setApplication($this);

        return $this;
    }

    public function addCommands(array $commands): self
    {
        foreach ($commands as $command) {
            $this->addCommand($command);
        }
        return $this;
    }

    public function getCommands(): array
    {
        return $this->commands;
    }

    private function init(): void
    {
        $commands = require_once dirname(__DIR__) . '/config/commands.php';
        $this->addCommands($commands);
        $this->input = new Input();
        $this->output = new Output();
    }

    public function getInput(): ?Input
    {
        return $this->input;
    }

    public function setInput(?Input $input): self
    {
        $this->input = $input;
        return $this;
    }

    public function getOutput(): ?Output
    {
        return $this->output;
    }

    public function setOutput(?Output $output): self
    {
        $this->output = $output;
        return $this;
    }

    public function run(): int
    {
        $commandName = $this->input->getCommandName();
        if (!$commandName) {
            return $this->commands['list']
                ->setInput($this->input)
                ->setOutput($this->output)
                ->execute();
        }

        if (!isset($this->commands[$commandName])) {
            throw new CommandNotFoundException('Command not found: ' . $commandName);
        }

        $arguments = $this->input->getArguments();
        if (array_search('help', $arguments) !== false) {
            $this->input->setArguments([$commandName]);
            return $this->commands['help']
                ->setInput($this->input)
                ->setOutput($this->output)
                ->execute();
        }

        return $this->commands[$commandName]
            ->setInput($this->input)
            ->setOutput($this->output)
            ->execute();
    }
}
