<?php

namespace MyConsole\Commands;

use MyConsole\Storages\CommandStorage;

/**
 * Команда позволяющая выводить описание каманд
 * Пример: help list или list {help}
 */
class HelpCommand extends Command
{
    protected ?string $name = 'help';

    protected ?string $description = 'Display help for a command';

    public function execute(): int
    {
        $arguments = $this->getInput()->getArguments();
        if (!$arguments) {
            return CommandStorage::INVALID;
        }

        $commands = $this->getApplication()->getCommands();
        $message =  'Command not found: ' . $arguments[0];
        $return = CommandStorage::INVALID;
        if (isset($commands[$arguments[0]])) {
            $message = $commands[$arguments[0]]->getDescription();
            $return  = CommandStorage::SUCCESS;
        }

        $this->getOutput()->writeln();
        $this->getOutput()->writeln($message);
        $this->getOutput()->writeln();

        return $return;
    }
}
