<?php

namespace MyConsole\Commands;

use MyConsole\Storages\CommandStorage;

/**
 * Команда позволяющая выводить список зарегистрированных команд
 */
class ListCommand extends Command
{
    protected ?string $name = 'list';

    protected ?string $description = 'Commands list output';

    public function execute(): int
    {
        $commands = $this->getApplication()->getCommands();

        $maxNameLen = 0;
        foreach (array_keys($commands) as $commandName) {
            $len = strlen($commandName);
            if ($len > $maxNameLen) {
                $maxNameLen = $len;
            }
        }

        $output = $this->getOutput();
        $output->writeln();
        foreach ($commands as $command) {
            $output->write(str_pad($command->getName() . ' ', $maxNameLen + 1, '-'));
            $output->write('------------- ');
            $output->writeln($command->getDescription());
        }
        $output->writeln();

        return CommandStorage::SUCCESS;
    }
}
