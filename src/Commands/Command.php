<?php

namespace MyConsole\Commands;

use MyConsole\Application;
use MyConsole\Helpers\SystemHelper;
use MyConsole\Input\Input;
use MyConsole\Output\Output;

/**
 * Базовый класс для команд, все зарегистрированные команды должны наследоваться от этого класса
 */
abstract class Command
{
    protected ?string $name = null;

    protected ?string $description = null;

    private ?Application $application = null;

    private ?Input $input = null;

    private ?Output $output = null;

    public function __construct(?string $name = null, ?string $description = null)
    {
        if (!$this->name) {
            $this->name = $name ?: SystemHelper::getNameClassInSnakeCase($this);
        }
        if (!$this->description) {
            $this->description = $description;
        }
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function getApplication(): ?Application
    {
        return $this->application;
    }

    public function setApplication(?Application $application): self
    {
        $this->application = $application;
        return $this;
    }

    public function getInput(): ?Input
    {
        return $this->input;
    }

    public function setInput(?Input $input): self
    {
        $this->input = $input;
        return $this;
    }

    public function getOutput(): ?Output
    {
        return $this->output;
    }

    public function setOutput(?Output $output): self
    {
        $this->output = $output;
        return $this;
    }

    /**
     * Обязательный метод, в котором описывается логика команды
     *
     * @return int
     */
    abstract public function execute(): int;
}
