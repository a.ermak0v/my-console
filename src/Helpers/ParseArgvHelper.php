<?php

namespace MyConsole\Helpers;

/**
 * Класс для помощи парсинга аргументов и параметров
 */
class ParseArgvHelper
{
    /**
     * Основной метод для парсинга
     *
     * @param array $argv
     * @return array
     */
    public static function parse(array $argv): array
    {
        array_shift($argv);

        $commandName = array_shift($argv);
        $arguments = [];
        $options = [];
        foreach ($argv as $item) {
            $option = self::parseOption($item);
            if ($option) {
                foreach ($option['values'] as $value) {
                    $options[$option['name']][] = $value;
                }
                continue;
            }

            $addArguments = self::parseArguments($item);
            if ($addArguments) {
                $arguments = array_merge($arguments, $addArguments);
                continue;
            }

            $arguments[] = $item;
        }

        return [$commandName, $arguments, $options];
    }

    private static function parseArguments(string $string): ?array
    {
        if (!self::isValidString($string, '{', '}')) {
            return null;
        }

        return explode(',', substr($string, 1, -1));
    }

    private static function parseOption(string $string): ?array
    {
        if (!self::isValidString($string, '[', ']')) {
            return null;
        }

        $array = explode('=', substr($string, 1, -1));
        if (count($array) < 2) {
            return null;
        }
        $name = array_shift($array);
        $value = implode('', $array);
        $values = self::parseArguments($value) ?: [$value];

        return [
            'name' => $name,
            'values' => $values,
        ];
    }

    private static function isValidString(string $string, string $firstItem, string $lastItem): bool
    {
        $len = strlen($string);
        if ($len < 3 || $string[0] !== $firstItem || $string[$len - 1] !== $lastItem) {
            return false;
        }
        return true;
    }
}
