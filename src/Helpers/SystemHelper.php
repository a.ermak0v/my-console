<?php

namespace MyConsole\Helpers;

use ReflectionClass;

class SystemHelper
{
    /**
     * Метод для получания названия класса в snake_case
     *
     * @param object $class
     * @return string
     */
    public static function getNameClassInSnakeCase(object $class): string
    {
        $nameClass = (new ReflectionClass($class))->getShortName();
        return ltrim(strtolower(preg_replace('/[A-Z]([A-Z](?![a-z]))*/', '_$0', $nameClass)), '_');
    }
}
