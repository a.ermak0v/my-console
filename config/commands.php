<?php

use MyConsole\Commands\HelpCommand;
use MyConsole\Commands\ListCommand;

/**
 * Конфиг с предустановленными командоми
 */
return [
    new HelpCommand(),
    new ListCommand(),
];
